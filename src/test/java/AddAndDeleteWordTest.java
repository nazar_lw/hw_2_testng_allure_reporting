import businesobject.AddAndDeleteWordBO;
import businesobject.LogInLogOutBO;
import io.qameta.allure.*;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.List;

import static source.PropertyFileHandler.*;

public class AddAndDeleteWordTest extends BaseTest {

    @Parameters("word")
    @Severity(SeverityLevel.MINOR)
    @Test
    @Description("Verify Addition and removal of new words")
    @Feature("Feature1: Add and remove a new word")
    @Story("Verify Addition and removal of new words")
    @Step("Verify Addition and removal of new words")
    public void verifyAddAndDeleteWord(@Optional("conscientious") String word) {

        LogInLogOutBO logInLogOutBO = new LogInLogOutBO();
        AddAndDeleteWordBO addAndDeleteWordBO = new AddAndDeleteWordBO();

        logInLogOutBO.goToPageURL(MAIN_URL);
        logInLogOutBO.logIn(EMAIL, PASSWORD);

        addAndDeleteWordBO.getToWordList();
        addAndDeleteWordBO.addWord(word);
        WebElement wordItem = addAndDeleteWordBO.getWordItemByWord(word);
        Assert.assertEquals(wordItem.getText(), word,
                "web element does not contain a word");

        addAndDeleteWordBO.deleteWord(word);
        List<String> allMyWords = addAndDeleteWordBO.getAllWords();
        Assert.assertFalse(allMyWords.contains(word),
                "List of words does not contain a word");

        logInLogOutBO.logOut();
    }
}
