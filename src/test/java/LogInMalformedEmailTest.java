import businesobject.LogInLogOutBO;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import static source.PropertyFileHandler.MAIN_URL;
import static source.PropertyFileHandler.PASSWORD;

public class LogInMalformedEmailTest extends BaseTest {

    @Severity(SeverityLevel.MINOR)
    @Test
    @Description("Verify LogInation with malformed email adress")
    @Feature("Feature1: LogIn with malformed email")
    @Story("Verify LogInation with malformed email adress")
    @Step("Verify LogInation with malformed email adress")
    public void verifyLoginationMalformedEmail() {
        LogInLogOutBO logInLogOutBO = new LogInLogOutBO();
        logInLogOutBO.goToPageURL(MAIN_URL);
        logInLogOutBO.logIn("NotAnEmailAtAll", PASSWORD);
        Assert.assertFalse(
                logInLogOutBO.isSubmitButtonEnabled(),
                "Submit button Enabled despite malformed email. ");
    }
}
