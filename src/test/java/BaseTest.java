import driver.DriverFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.lang.reflect.Method;

import static source.PropertyFileHandler.CHROME_DRIVER;

@Listeners({CustomListeners.class})
public abstract class BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        DriverFactory.buildDriver(CHROME_DRIVER);
    }

    @AfterMethod
    public void afterMethod(final Method method) {
        DriverFactory.quitDriver();
    }
}