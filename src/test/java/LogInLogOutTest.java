import businesobject.LogInLogOutBO;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import static source.PropertyFileHandler.*;

public class LogInLogOutTest extends BaseTest {

    @Severity(SeverityLevel.MINOR)
    @Test
    @Description("Verify LogIn and LogOut processes")
    @Feature("Feature1: LogIn @ LogOut")
    @Story("Verify LogIn and LogOut processes")
    @Step("Verify LogIn and LogOut processes")
    public void verifyLogination() {

        LogInLogOutBO logInLogOutBO = new LogInLogOutBO();
        logInLogOutBO.goToPageURL(MAIN_URL);
        logInLogOutBO.logIn(EMAIL, PASSWORD);

        Assert.assertEquals(
                logInLogOutBO.getUserCredentials().getText(), LOGIN,
                "Actual user credentials are not equal to expected.  ");
        logInLogOutBO.logOut();
    }
}
