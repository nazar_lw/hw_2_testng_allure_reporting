import businesobject.GrammarSectionBO;
import businesobject.LogInLogOutBO;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static source.PropertyFileHandler.*;

public class GrammarSectionTest extends BaseTest {

    @Severity(SeverityLevel.MINOR)
    @Test
    @Description("Verify Usage of grammar premium sections ")
    @Feature("Feature1: Grammar premium sections")
    @Story("Verify Usage of grammar premium sections")
    @Step("Verify Usage of grammar premium sections")
    public void verifyGrammarPremiumSections() {

        LogInLogOutBO logInLogOutBO = new LogInLogOutBO();
        GrammarSectionBO grammarSectionBO = new GrammarSectionBO();

        logInLogOutBO.goToPageURL(MAIN_URL);
        logInLogOutBO.logIn(EMAIL, PASSWORD);
        grammarSectionBO.getToGrammarSection();

        List<Integer> indexes = grammarSectionBO.getIndexesOfPremiumCards();
        List<String> textsOnPremiumCards = grammarSectionBO.getTextsPremiumCards(indexes);
        List<String> expectedPremiumGrammarTopics = Arrays.asList(PRIME_GRAMMAR_TOPICS);
        logInLogOutBO.logOut();

        Assert.assertTrue((textsOnPremiumCards.size() == expectedPremiumGrammarTopics.size()) &&
                        expectedPremiumGrammarTopics.containsAll(textsOnPremiumCards),
                "Content of Grammar Premium topics does not match to expected. ");
    }
}
