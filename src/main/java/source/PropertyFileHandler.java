package source;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static logger.AllureLogger.logToAllureInfo;

public class PropertyFileHandler {
    public static final String MAIN_URL;
    public static final String EMAIL;
    public static final String PASSWORD;
    public static final String LOGIN;
    public static final String CHROME_DRIVER;
    public static final String GECKO_DRIVER;
    public static final String CHROME_DRIVER_PATH;
    public static final String GECKO_DRIVER_PATH;
    public static final String[] PRIME_GRAMMAR_TOPICS;

    static {
        Properties prop = new Properties();
        try (InputStream input =
                     new FileInputStream("src\\main\\resources\\config.properties")) {
            prop.load(input);
            logToAllureInfo("Constants are extracted from the property file");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        MAIN_URL = prop.getProperty("mainurl");
        EMAIL = prop.getProperty("email");
        PASSWORD = prop.getProperty("password");
        LOGIN = prop.getProperty("login");
        CHROME_DRIVER = prop.getProperty("chromedriver");
        GECKO_DRIVER = prop.getProperty("geckodriver");
        GECKO_DRIVER_PATH = prop.getProperty("geckodriverPath");
        CHROME_DRIVER_PATH = prop.getProperty("chromedriverPath");
        PRIME_GRAMMAR_TOPICS = prop.getProperty("primeGrammarTopics").split("#");
    }

}
