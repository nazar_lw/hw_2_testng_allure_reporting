package businesobject;

import driver.DriverFactory;
import org.openqa.selenium.WebElement;
import pageobject.LogInLogOutPO;

import static pageobject.AbstractPO.getElementWithLoop;
import static pageobject.AbstractPO.isElementEnabled;

public class LogInLogOutBO {

    private LogInLogOutPO logInLogOutPO = new LogInLogOutPO();

    public void goToPageURL(String url) {
        logInLogOutPO.goToPageURL(url);
    }

    public void logIn(String email, String password) {
        logInLogOutPO.chooseLanguage();
        logInLogOutPO.alreadyHaveAccount();
        logInLogOutPO.inputEmail(email);
        logInLogOutPO.inputPassword(password);
        logInLogOutPO.submit();
        logInLogOutPO.closeAd();
    }

    public WebElement getUserCredentials() {
        logInLogOutPO.showPopupPersonalData(DriverFactory.getDriver());
        return getElementWithLoop(logInLogOutPO.loginHolder);
    }

    public void logOut() {
        logInLogOutPO.showPopupPersonalData(DriverFactory.getDriver());
        logInLogOutPO.logOut();
    }

    public boolean isSubmitButtonEnabled() {
        return isElementEnabled(logInLogOutPO.submitButton);
    }
}
