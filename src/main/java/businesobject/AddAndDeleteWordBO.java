package businesobject;

import org.openqa.selenium.WebElement;
import pageobject.AddAndDeleteWordPO;

import java.util.List;
import java.util.stream.Collectors;

public class AddAndDeleteWordBO {

    private AddAndDeleteWordPO addAndDeleteWordPO = new AddAndDeleteWordPO();

    public void getToWordList() {
        addAndDeleteWordPO.chooseVocabulary();
        addAndDeleteWordPO.chooseMyWordList();
    }

    public void addWord(String word) {
        addAndDeleteWordPO.setWord(word);
        addAndDeleteWordPO.pressAddWord();
        addAndDeleteWordPO.chooseFirstTranslation();
        //addAndDeleteWordPO.showListNewWords();
    }

    public List<String> getAllWords() {
        return addAndDeleteWordPO.getVocabularyElements().
                stream().map(WebElement::getText).
                collect(Collectors.toList());
    }

    public WebElement getWordItemByWord(String word) {
        return addAndDeleteWordPO.wordItemByWord(word);
    }

    public void deleteWord(String word) {
        addAndDeleteWordPO.pressDeleteIcon(word);
        addAndDeleteWordPO.confirmDelete();
    }

}
