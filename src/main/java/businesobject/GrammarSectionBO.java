package businesobject;

import org.openqa.selenium.WebElement;
import pageobject.GrammarSectionPO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GrammarSectionBO {

    private GrammarSectionPO grammarSectionPO = new GrammarSectionPO();

    public void getToGrammarSection() {
        grammarSectionPO.getToLearnSection();
        grammarSectionPO.getMiddleGrammarLevel();
    }

    public List<Integer> getIndexesOfPremiumCards() {

        List<WebElement> allPremiumIcons = grammarSectionPO.getElementsIconHolders();
        List<Integer> indexesOfPremiumEnabledCards = new ArrayList<>();
        for (WebElement premiumIcon : allPremiumIcons) {
            if (premiumIcon.isDisplayed()) {
                indexesOfPremiumEnabledCards.add(allPremiumIcons.indexOf(premiumIcon));
            }
        }
        return indexesOfPremiumEnabledCards;
    }

    public List<String> getTextsPremiumCards(List<Integer> indexes) {

        List<WebElement> allTestHolderCards = grammarSectionPO.getElementsTextHolders();
        List<WebElement> testHoldersWithPremium = new ArrayList<>();
        for (Integer index : indexes) {
            testHoldersWithPremium.add(allTestHolderCards.get(index));
        }
        return testHoldersWithPremium.stream().
                map(WebElement::getText).collect(Collectors.toList());
    }
}
