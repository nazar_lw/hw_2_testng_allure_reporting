package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AddAndDeleteWordPO extends AbstractPO {

    private By vocabulary =
            By.xpath("//a[@href=\"/ru/dictionary\"]/span/span[2]");
    private By myWordList = By.cssSelector("a.ll-leokit__card__link");
    private By wordInput = By.cssSelector("input.ll-leokit__input__m-empty");
    private By addWordButton = By.cssSelector("span.ll-page-vocabulary__search-btn");
    private By firstTranslation =
            By.cssSelector("div.ll-leokit__translations-list-item:nth-of-type(1)");
    private By listNewWords = By.xpath("//span[@data-arg=\"new\"]");
    private By wordItemList = By.cssSelector("strong.sets-words__my-word");
    private By confirmDelete = By.xpath("//div[@class=\"ll-leokit__modal-confirmation__btn\"][1]");

    public void chooseVocabulary() {
        getElement(vocabulary).click();
    }

    public void chooseMyWordList() {
        getElement(myWordList).click();
    }

    public void setWord(String word) {
        getElement(wordInput).sendKeys(word);
    }

    public void pressAddWord() {
        getElement(addWordButton).click();
    }

    public void chooseFirstTranslation() {
        getElement(firstTranslation).click();
    }

    public void showListNewWords() { getElement(listNewWords).click(); }

    public List<WebElement> getVocabularyElements() {
        return getElements(wordItemList);
    }

    public WebElement wordItemByWord(String word) {
        return getElementWithLoop(By.xpath(String.format("//strong[@class='sets-words__my-word'][text()='%s']", word)));
    }

    public void pressDeleteIcon(String word) {
        getElement(By.xpath(String.format(
                "//strong[@class='sets-words__my-word'][text()='%s']/ancestor::div[@class='sets-words__row false']/div[7]/div/span\n",
                word))).
                click();
    }

    public void confirmDelete() {
        getElement(confirmDelete).click();
    }

}
