package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import static logger.AllureLogger.logToAllureInfo;

public class LogInLogOutPO extends AbstractPO {

    private By chooseLanguage =
            By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div/div/div[2]/div/a[2]/span/span");

    private By alreadyHaveAccount =
            By.cssSelector("div.ll-login-form:nth-child(6)");

    private By mailInput = By.cssSelector("input[placeholder=Email]");

    private By passwordInput = By.cssSelector("input[placeholder=Password]");

    public By submitButton = By.cssSelector("div.ll-login-form>button.ll-button.ll-button__m-color-green");

    private By closeModalWithAd = By.id("modal-close");

    private By popupPersonalData = By.cssSelector("div.ll-leokit__site-menu__user-menu-toggler");

    public By loginHolder = By.cssSelector("div.ll-leokit__user-menu__profile__name");

    private By logOut =
            By.xpath("//span[text()='Выход'][@class='ll-leokit__user-menu-item__content__title']");

    public void chooseLanguage() {
        getElement(chooseLanguage).click();
    }

    public void alreadyHaveAccount() {
        getElement(alreadyHaveAccount).click();
    }

    public void inputEmail(String email) {
        getElement(mailInput).sendKeys(email);
    }

    public void inputPassword(String password) {
        getElement(passwordInput).sendKeys(password);
    }

    public void submit() {
        getElement(submitButton).click();
    }

    public void closeAd() {
        if (isDisplayed(closeModalWithAd)) {
            System.out.println("closeModalWithAd is DISPALYED!!!");
            executeElementWithTimeout(closeModalWithAd);
        }
    }

    public void showPopupPersonalData(WebDriver driver) {
        Actions builder = new Actions(driver);
        builder.moveToElement(getElement(popupPersonalData)).build().perform();
    }

    public void logOut() {
        getElement(logOut).click();
        logToAllureInfo("LogOut off the app");
    }
}
