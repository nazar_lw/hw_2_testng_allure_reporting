package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GrammarSectionPO extends AbstractPO {

    private By buttonLearn =
            By.xpath("//div[@class=' ll-leokit__site-menu-top-item']/a[@href='/ru/training']");

    private By middleGrammarLevel =
            By.xpath("//a[@href='/ru/grammar/rules#middle']");

    private By grammarItemsText =
            By.xpath("//*[@id=\"middle\"]/div/div[@class='ll-leokit__kit-layer']/a/div/div[@class='ll-leokit__grammar-training-card__text']/div[1]");

    private By grammarItemsIcons =
            By.xpath("//*[@id=\"middle\"]/div/div[@class='ll-leokit__kit-layer']/a/div/div[@class='ll-leokit__grammar-training-card__gold']");

    public void getToLearnSection() {
        getElementWithLoop(buttonLearn).click();
    }

    public void getMiddleGrammarLevel() {
        getElement(middleGrammarLevel).click();
    }

    public List<WebElement> getElementsTextHolders() {
        return getElements(grammarItemsText);
    }

    public List<WebElement> getElementsIconHolders() {
        return getElements(grammarItemsIcons);
    }
}
