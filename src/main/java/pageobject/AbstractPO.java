package pageobject;

import driver.DriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.List;

import static logger.AllureLogger.logToAllureInfo;

public abstract class AbstractPO {

    private static FluentWait<WebDriver> newWait() {
        return new FluentWait<>(DriverFactory.getDriver())
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
    }

    public void goToPageURL(final String url) {
        DriverFactory.getDriver().get(url);
        logToAllureInfo("Going to URL: " + url);
    }

    public static WebElement getElement(By locator) {
        logToAllureInfo("Getting element by: ( " + locator + " )");
        return newWait().until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement getElementWithLoop(By locator) {
        WebElement webElement = null;
        int i = 1;
        while (i < 10) {
            try {
                webElement =
                        newWait().until(ExpectedConditions.visibilityOfElementLocated(locator));
                i = 10;
            } catch (Exception e) {
                logToAllureInfo("While loop on element, iterator: " + 1);
                i++;
            }
        }
        return webElement;
    }

    static void executeElementWithTimeout(By locator) {
        try {
            Thread.sleep(3000);
            newWait().until(ExpectedConditions.visibilityOfElementLocated(locator)).click();
            logToAllureInfo("Executing timeout");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static List<WebElement> getElements(By locator) {
        logToAllureInfo("Getting collections of elements by: ( " + locator + " )");
        return DriverFactory.getDriver().findElements(locator);
    }

    public static boolean isDisplayed(By locator) {
        try {
            logToAllureInfo("Checking if element: ( " + locator + " ) is displayed");
            return DriverFactory
                    .getDriver()
                    .findElement(locator)
                    .isDisplayed();
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }

    public static boolean isElementEnabled(By locator) {
        try {
            logToAllureInfo("Checking if element: ( " + locator + " ) is enabled");
            return DriverFactory
                    .getDriver()
                    .findElement(locator)
                    .isEnabled();
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }
}
